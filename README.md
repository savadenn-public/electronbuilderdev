# electronbuilderdev

To run electron:serve inside docker and redirect x11

## Usage

```console
docker run --rm -it \
    --env ELECTRON_CACHE="/app/.cache/electron" \
    --env ELECTRON_BUILDER_CACHE="/app/.cache/electron-builder" \
    --env YARN_CACHE_FOLDER="/app/.cache/yarn" \
    --env="DISPLAY" \
    --volume="$(pwd):/app" \
    --volume="${HOME}/.Xauthority:/root/.Xauthority:rw" \
    -it --net=host  \
    -u "$(id -u):$(id -g)" \
    -w "/app" \
   savadenn/electronbuilderdev bash -c "yarn && yarn electron:serve"
```
