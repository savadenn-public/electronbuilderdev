ARG NODE_VERSION=latest

FROM node:${NODE_VERSION}

RUN apt update \
    && apt install -yqq \
        libnss3 \
        libgtk-3-0 \
        libxss1 \
        libasound2
